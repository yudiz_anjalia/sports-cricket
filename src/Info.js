import React from 'react'
import  data  from './data/cricket.json'
import './App.css'
import moment from "moment";


function Info() {

    const matchDate = moment(data.data.sport_event.scheduled).format("DD MMMM YYYY");
    // console.log("GSK" , data.data.sport_event.competitors[0].name)

      return(
      <div className='card'>         
             <div className='main'>
              <div className='title'>
                  <div className='left-title'>
              <p className='IPL'>{data.data.sport_event.season.name}</p>
              <h1 className='GTCSK'>{data.data.sport_event.competitors[0].name} vS {data.data.sport_event.competitors[1].name}</h1>
              </div>
              <div>
              <div className='right-title'>
                  <div className='rdata'>
                    {data.data.sport_event.venue.name} , {data.data.sport_event.venue.city_name} | </div>
                  <div className='date'>
                    {matchDate} 
                    </div>
                <div className='result'>
                    {data.data.sport_event_status.equation}
                </div>
               </div>
           </div>
          </div>

          <div className='main-second'>
            <div className='teams'>
                <div className='team1'>
                <div> <img src='https://media.sports.info/logos/sr:competitor:152334.pnggit in'/> </div>
                </div>
            </div>
          </div>
          </div>
          </div>

      )
  
}




export default Info